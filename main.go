package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

func generate(w http.ResponseWriter, r *http.Request) {
	file := GetFileFromS3()
	w.Header().Set("Content-Disposition", "attachment; filename="+file.Name())
	w.Header().Set("Content-Type", r.Header.Get("Content-Type"))
	io.Copy(w, file)
	os.Remove(file.Name())
}

func healthcheck(w http.ResponseWriter, r *http.Request) {
	log.Print("Health OK")
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "hello_universe_big_bang")
}

func main() {
	http.HandleFunc("/generate", generate)
	http.HandleFunc("/health", healthcheck)
	http.HandleFunc("/status", handler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
