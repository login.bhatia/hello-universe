# https://www.terraform.io/docs/language/values/variables.html

###### Project ######
variable "aws_region" {
  type = string
}

variable "project_name" {
  type = string
}

variable "environment" {
  type = string
}

###### Load Balancer ######
variable "lb_target_port" {
  type = number
}
variable "lb_listener_port" {
  type = number
}

variable "lb_listener_protocol" {
  type = string
}

variable "lb_target_protocol" {
  type = string
}

variable "lb_target_health" {
  type = string
}

###### Elastic Container Service ######
variable "ecs_service_desired_task_count" {
  type = number
}

variable "ecs_service_health_check_grace_period_seconds" {
  type = number
}
variable "ecs_task_cpu" {
  type = string
}

variable "ecs_task_memory" {
  type = string
}

variable "log_retention_days" {
  type = number
}