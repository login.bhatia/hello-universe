resource "aws_ecs_task_definition" "tf_resource_ecs_task" {
  family                = "${var.project_name}_${var.environment}"
  container_definitions = <<TASK_DEFINITION
[
    {
        "environment": [
            {"name": "VARNAME", "value": "VARVAL"}
        ],
        "image": "registry.gitlab.com/login.bhatia/hello-universe/generate-project:latest",
        "name": "${var.project_name}_${var.environment}_container",
        "portMappings": [
            {
                "containerPort": ${var.lb_target_port}
            }
        ],
        "LogConfiguration" : {
                                "LogDriver" : "awslogs",
                                "Options" : {
                                    "awslogs-group": "${var.project_name}/backend/${var.environment}/cluster",
                                    "awslogs-region": "${var.aws_region}",
                                    "awslogs-stream-prefix": "${var.project_name}_${var.environment}_service"
                                }
                              }
    }
]
TASK_DEFINITION

  cpu                      = var.ecs_task_cpu
  execution_role_arn       = aws_iam_role.tf_resource_role_ecs_execution.arn
  task_role_arn            = aws_iam_role.tf_resource_role_ecs_task.arn
  memory                   = var.ecs_task_memory
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]

}
resource "aws_ecs_cluster" "tf_resource_ecs_cluster" {
  name = "${var.project_name}_${var.environment}_cluster"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}
resource "aws_ecs_service" "tf_resource_ecs_service" {
  name                              = "${var.project_name}_${var.environment}_service"
  cluster                           = aws_ecs_cluster.tf_resource_ecs_cluster.id
  task_definition                   = aws_ecs_task_definition.tf_resource_ecs_task.arn
  desired_count                     = var.ecs_service_desired_task_count
  launch_type                       = "FARGATE"
  health_check_grace_period_seconds = var.ecs_service_health_check_grace_period_seconds

  load_balancer {
    target_group_arn = aws_lb_target_group.tf_resource_tg_backend.arn
    container_name   = "${var.project_name}_${var.environment}_container"
    container_port   = var.lb_target_port
  }

  network_configuration {
    subnets = [data.aws_ssm_parameter.tf_data_parameter_private_subnet_a_1_id.value,
      data.aws_ssm_parameter.tf_data_parameter_private_subnet_b_1_id.value,
      data.aws_ssm_parameter.tf_data_parameter_private_subnet_c_1_id.value,
      data.aws_ssm_parameter.tf_data_parameter_private_subnet_d_1_id.value,
      data.aws_ssm_parameter.tf_data_parameter_private_subnet_e_1_id.value,
    data.aws_ssm_parameter.tf_data_parameter_private_subnet_f_1_id.value, ]
    security_groups = [aws_security_group.tf_resource_sg_ecs_service_backend.id]
  }
}
