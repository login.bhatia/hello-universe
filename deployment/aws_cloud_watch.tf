resource "aws_cloudwatch_log_group" "tf_resource_log_group_backend" {
  retention_in_days = var.log_retention_days
  name              = "${var.project_name}/backend/${var.environment}/cluster"
}