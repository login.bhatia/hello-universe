resource "aws_lb_target_group" "tf_resource_tg_backend" {
  port        = var.lb_target_port
  protocol    = var.lb_target_protocol
  vpc_id      = data.aws_ssm_parameter.tf_data_parameter_vpc_id.value
  target_type = "ip"
  health_check {
    path = var.lb_target_health
    port = var.lb_target_port
  }
}

resource "aws_lb" "tf_resource_alb_backend" {
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.tf_resource_sg_lb_backend.id]
  subnets = [data.aws_ssm_parameter.tf_data_parameter_public_subnet_a_1_id.value,
    data.aws_ssm_parameter.tf_data_parameter_public_subnet_b_1_id.value,
    data.aws_ssm_parameter.tf_data_parameter_public_subnet_c_1_id.value,
    data.aws_ssm_parameter.tf_data_parameter_public_subnet_d_1_id.value,
    data.aws_ssm_parameter.tf_data_parameter_public_subnet_e_1_id.value,
  data.aws_ssm_parameter.tf_data_parameter_public_subnet_f_1_id.value, ]
}

resource "aws_lb_listener" "tf_resource_listener_backend" {
  load_balancer_arn = aws_lb.tf_resource_alb_backend.arn
  port              = var.lb_listener_port
  protocol          = var.lb_listener_protocol

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tf_resource_tg_backend.arn
  }
}
