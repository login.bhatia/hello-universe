# Store values in AWS Systems Manager Parameter Store

###### VPC Id ######
data "aws_ssm_parameter" "tf_data_parameter_vpc_id" {
  name  = "/${var.environment}/vpc_id"
}

###### Private Subnet Ids ######
data "aws_ssm_parameter" "tf_data_parameter_private_subnet_a_1_id" {
  name  = "/${var.environment}/private_subnet_a_1_id"
}

data "aws_ssm_parameter" "tf_data_parameter_private_subnet_b_1_id" {
  name  = "/${var.environment}/private_subnet_b_1_id"
}

data "aws_ssm_parameter" "tf_data_parameter_private_subnet_c_1_id" {
  name  = "/${var.environment}/private_subnet_c_1_id"
}

data "aws_ssm_parameter" "tf_data_parameter_private_subnet_d_1_id" {
  name  = "/${var.environment}/private_subnet_d_1_id"
}

data "aws_ssm_parameter" "tf_data_parameter_private_subnet_e_1_id" {
  name  = "/${var.environment}/private_subnet_e_1_id"
}

data "aws_ssm_parameter" "tf_data_parameter_private_subnet_f_1_id" {
  name  = "/${var.environment}/private_subnet_f_1_id"
}

###### Public Subnet Ids ######
data "aws_ssm_parameter" "tf_data_parameter_public_subnet_a_1_id" {
  name  = "/${var.environment}/public_subnet_a_1_id"
}

data "aws_ssm_parameter" "tf_data_parameter_public_subnet_b_1_id" {
  name  = "/${var.environment}/public_subnet_b_1_id"
}

data "aws_ssm_parameter" "tf_data_parameter_public_subnet_c_1_id" {
  name  = "/${var.environment}/public_subnet_c_1_id"
}

data "aws_ssm_parameter" "tf_data_parameter_public_subnet_d_1_id" {
  name  = "/${var.environment}/public_subnet_d_1_id"
}

data "aws_ssm_parameter" "tf_data_parameter_public_subnet_e_1_id" {
  name  = "/${var.environment}/public_subnet_e_1_id"
}

data "aws_ssm_parameter" "tf_data_parameter_public_subnet_f_1_id" {
  name  = "/${var.environment}/public_subnet_f_1_id"
}