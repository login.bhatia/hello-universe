###### Project ######
aws_region   = "us-east-1"
project_name = "hello_universe"
environment  = "staging"

###### Load Balancer ######
lb_target_port       = 8080
lb_listener_port     = 80
lb_listener_protocol = "HTTP"
lb_target_protocol   = "HTTP"
lb_target_health     = "/health"

###### Elastic Container Service ######
ecs_service_desired_task_count                = 1
ecs_service_health_check_grace_period_seconds = 10
ecs_task_cpu                                  = "256"
ecs_task_memory                               = "512"
log_retention_days                            = 1