terraform {

  # Inspired From:  https://www.youtube.com/watch?v=iGXjUrkkzDI&t=1s
  # Use Gitlab to store terraform state remotely
  backend "http" {
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = var.aws_region
  default_tags {
    tags = {
      Environment = var.environment
      Project     = var.project_name
    }
  }
}
