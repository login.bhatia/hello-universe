resource "aws_security_group" "tf_resource_sg_lb_backend" {
  vpc_id = data.aws_ssm_parameter.tf_data_parameter_vpc_id.value
  ingress {
    from_port   = var.lb_listener_port
    to_port     = var.lb_listener_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_security_group" "tf_resource_sg_ecs_service_backend" {
  vpc_id = data.aws_ssm_parameter.tf_data_parameter_vpc_id.value
  ingress {
    from_port       = var.lb_target_port
    to_port         = var.lb_target_port
    protocol        = "tcp"
    security_groups = [aws_security_group.tf_resource_sg_lb_backend.id]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}
