# https://www.terraform.io/docs/language/values/variables.html

# Project
project_name = "hello_universe"
aws_region   = "us-east-1"
environment  = "staging"