# https://www.terraform.io/docs/language/values/variables.html

# Project
variable "aws_region" {
  type = string
}

variable "project_name" {
  type = string
}

variable "environment" {
  type = string
}