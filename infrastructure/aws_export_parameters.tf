# Store values in AWS Systems Manager Parameter Store

###### VPC Id ######
resource "aws_ssm_parameter" "tf_resource_parameter_vpc_id" {
  name  = "/${var.environment}/vpc_id"
  type  = "String"
  value = aws_vpc.tf_resource_vpc.id
}

###### Private Subnet Ids ######
resource "aws_ssm_parameter" "tf_resource_parameter_private_subnet_a_1_id" {
  name  = "/${var.environment}/private_subnet_a_1_id"
  type  = "String"
  value = aws_subnet.private_subnet_a_1.id
}

resource "aws_ssm_parameter" "tf_resource_parameter_private_subnet_b_1_id" {
  name  = "/${var.environment}/private_subnet_b_1_id"
  type  = "String"
  value = aws_subnet.private_subnet_b_1.id
}

resource "aws_ssm_parameter" "tf_resource_parameter_private_subnet_c_1_id" {
  name  = "/${var.environment}/private_subnet_c_1_id"
  type  = "String"
  value = aws_subnet.private_subnet_c_1.id
}

resource "aws_ssm_parameter" "tf_resource_parameter_private_subnet_d_1_id" {
  name  = "/${var.environment}/private_subnet_d_1_id"
  type  = "String"
  value = aws_subnet.private_subnet_d_1.id
}

resource "aws_ssm_parameter" "tf_resource_parameter_private_subnet_e_1_id" {
  name  = "/${var.environment}/private_subnet_e_1_id"
  type  = "String"
  value = aws_subnet.private_subnet_e_1.id
}

resource "aws_ssm_parameter" "tf_resource_parameter_private_subnet_f_1_id" {
  name  = "/${var.environment}/private_subnet_f_1_id"
  type  = "String"
  value = aws_subnet.private_subnet_f_1.id
}

###### Public Subnet Ids ######
resource "aws_ssm_parameter" "tf_resource_parameter_public_subnet_a_1_id" {
  name  = "/${var.environment}/public_subnet_a_1_id"
  type  = "String"
  value = aws_subnet.public_subnet_a_1.id
}

resource "aws_ssm_parameter" "tf_resource_parameter_public_subnet_b_1_id" {
  name  = "/${var.environment}/public_subnet_b_1_id"
  type  = "String"
  value = aws_subnet.public_subnet_b_1.id
}

resource "aws_ssm_parameter" "tf_resource_parameter_public_subnet_c_1_id" {
  name  = "/${var.environment}/public_subnet_c_1_id"
  type  = "String"
  value = aws_subnet.public_subnet_c_1.id
}

resource "aws_ssm_parameter" "tf_resource_parameter_public_subnet_d_1_id" {
  name  = "/${var.environment}/public_subnet_d_1_id"
  type  = "String"
  value = aws_subnet.public_subnet_d_1.id
}

resource "aws_ssm_parameter" "tf_resource_parameter_public_subnet_e_1_id" {
  name  = "/${var.environment}/public_subnet_e_1_id"
  type  = "String"
  value = aws_subnet.public_subnet_e_1.id
}

resource "aws_ssm_parameter" "tf_resource_parameter_public_subnet_f_1_id" {
  name  = "/${var.environment}/public_subnet_f_1_id"
  type  = "String"
  value = aws_subnet.public_subnet_f_1.id
}
