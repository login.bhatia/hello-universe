# AWS / RFC Recommendations to be followed while choosing CIDR range for Private IP Addresses
# https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Subnets.html#VPC_Sizing
# https://acloudguru-content-attachment-production.s3-accelerate.amazonaws.com/1597961645600-C02L05_Subnets_VPC_Routers_Route_Tables_Part1.pdf
# https://acloudguru-content-attachment-production.s3-accelerate.amazonaws.com/1597962316413-C02L06_Subnets_VPC_Routers_Route_Tables_Part2.pdf

# /16 VPC address space = 65,536 addresses (2^(32-16))
# Subnet Planning: 32 subnets (/21) in total with 2048 IPs (available 2043 per subnet)

#   In this file: case of us-east-1 (6 AZs)
#   8 - /19 subnet boundaries planned, 6 used across 6 AZs, 2 for future
#   4 - /21 subnets per AZ, 2 created, 2 for future

# 10.0.0.0/16
#   10.0.0.0/19             
#           10.0.0.0/21      : Used
#           10.0.8.0/21      : Used
#           10.0.16.0/21     : Future
#           10.0.24.0/21     : Future
#   10.0.32.0/19
#           10.0.32.0/21     : Used
#           10.0.40.0/21     : Used
#           10.0.48.0/21     : Future
#           10.0.56.0/21     : Future
#   10.0.64.0/19
#           10.0.64.0/21     : Used
#           10.0.72.0/21     : Used
#           10.0.80.0/21     : Future
#           10.0.88.0/21     : Future
#   10.0.96.0/19
#           10.0.96.0/21     : Used
#           10.0.104.0/21    : Used
#           10.0.112.0/21    : Future
#           10.0.120.0/21    : Future
#   10.0.128.0/19
#           10.0.128.0/21    : Used
#           10.0.136.0/21    : Used
#           10.0.144.0/21    : Future
#           10.0.152.0/21    : Future
#   10.0.160.0/19
#           10.0.160.0/21    : Used
#           10.0.168.0/21    : Used
#           10.0.176.0/21    : Future
#           10.0.184.0/21    : Future
#   10.0.192.0/19            : Future
#           10.0.192.0/21
#           10.0.200.0/21
#           10.0.208.0/21
#           10.0.216.0/21
#   10.0.224.0/19            : Future
#           10.0.224.0/21
#           10.0.232.0/21
#           10.0.240.0/21
#           10.0.248.0/21

resource "aws_vpc" "tf_resource_vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "${var.project_name}_${var.environment}_vpc"
  }
}

# Internet Gateway

resource "aws_internet_gateway" "tf_resource_igw" {
  vpc_id = aws_vpc.tf_resource_vpc.id

  tags = {
    Name = "${var.project_name}_${var.environment}_igw"
  }
}

# NAT Gateway

resource "aws_eip" "tf_resource_elastic_ip_nat" {
  vpc = true
}

resource "aws_nat_gateway" "tf_resource_nat_1" {
  allocation_id = aws_eip.tf_resource_elastic_ip_nat.id
  subnet_id     = aws_subnet.public_subnet_a_1.id
  depends_on = [
    aws_internet_gateway.tf_resource_igw
  ]
}

# Subnets and their Route Tables
# As per best practice, each subnet is associated with an independent route table
# Main route table associated with VPC is fallback

############### private_subnet_a_1 ##################

resource "aws_subnet" "private_subnet_a_1" {
  vpc_id            = aws_vpc.tf_resource_vpc.id
  cidr_block        = "10.0.0.0/21"
  availability_zone = "us-east-1a"

  tags = {
    Name = "${var.project_name}_${var.environment}_private_subnet_a_1"
  }
}

resource "aws_route_table" "rt_private_subnet_a_1" {
  vpc_id = aws_vpc.tf_resource_vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.tf_resource_nat_1.id
  }

  tags = {
    Name = "${var.project_name}_${var.environment}_rt_private_subnet_a_1"
  }
}

resource "aws_route_table_association" "rta_private_subnet_a_1" {
  subnet_id      = aws_subnet.private_subnet_a_1.id
  route_table_id = aws_route_table.rt_private_subnet_a_1.id
}

############### public_subnet_a_1 ##################

resource "aws_subnet" "public_subnet_a_1" {
  vpc_id            = aws_vpc.tf_resource_vpc.id
  cidr_block        = "10.0.8.0/21"
  availability_zone = "us-east-1a"

  tags = {
    Name = "${var.project_name}_${var.environment}_public_subnet_a_1"
  }
}

resource "aws_route_table" "rt_public_subnet_a_1" {
  vpc_id = aws_vpc.tf_resource_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.tf_resource_igw.id
  }

  tags = {
    Name = "${var.project_name}_${var.environment}_rt_public_subnet_a_1"
  }
}

resource "aws_route_table_association" "rta_public_subnet_a_1" {
  subnet_id      = aws_subnet.public_subnet_a_1.id
  route_table_id = aws_route_table.rt_public_subnet_a_1.id
}

############### private_subnet_b_1 ##################

resource "aws_subnet" "private_subnet_b_1" {
  vpc_id            = aws_vpc.tf_resource_vpc.id
  cidr_block        = "10.0.32.0/21"
  availability_zone = "us-east-1b"

  tags = {
    Name = "${var.project_name}_${var.environment}_private_subnet_b_1"
  }
}

resource "aws_route_table" "rt_private_subnet_b_1" {
  vpc_id = aws_vpc.tf_resource_vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.tf_resource_nat_1.id
  }

  tags = {
    Name = "${var.project_name}_${var.environment}_rt_private_subnet_b_1"
  }
}

resource "aws_route_table_association" "rta_private_subnet_b_1" {
  subnet_id      = aws_subnet.private_subnet_b_1.id
  route_table_id = aws_route_table.rt_private_subnet_b_1.id
}

############### public_subnet_b_1 ##################

resource "aws_subnet" "public_subnet_b_1" {
  vpc_id            = aws_vpc.tf_resource_vpc.id
  cidr_block        = "10.0.40.0/21"
  availability_zone = "us-east-1b"

  tags = {
    Name = "${var.project_name}_${var.environment}_public_subnet_b_1"
  }
}

resource "aws_route_table" "rt_public_subnet_b_1" {
  vpc_id = aws_vpc.tf_resource_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.tf_resource_igw.id
  }

  tags = {
    Name = "${var.project_name}_${var.environment}_rt_public_subnet_b_1"
  }
}

resource "aws_route_table_association" "rta_public_subnet_b_1" {
  subnet_id      = aws_subnet.public_subnet_b_1.id
  route_table_id = aws_route_table.rt_public_subnet_b_1.id
}

############### private_subnet_c_1 ##################

resource "aws_subnet" "private_subnet_c_1" {
  vpc_id            = aws_vpc.tf_resource_vpc.id
  cidr_block        = "10.0.64.0/21"
  availability_zone = "us-east-1c"

  tags = {
    Name = "${var.project_name}_${var.environment}_private_subnet_c_1"
  }
}

resource "aws_route_table" "rt_private_subnet_c_1" {
  vpc_id = aws_vpc.tf_resource_vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.tf_resource_nat_1.id
  }

  tags = {
    Name = "${var.project_name}_${var.environment}_rt_private_subnet_c_1"
  }
}

resource "aws_route_table_association" "rta_private_subnet_c_1" {
  subnet_id      = aws_subnet.private_subnet_c_1.id
  route_table_id = aws_route_table.rt_private_subnet_c_1.id
}

############### public_subnet_c_1 ##################

resource "aws_subnet" "public_subnet_c_1" {
  vpc_id            = aws_vpc.tf_resource_vpc.id
  cidr_block        = "10.0.72.0/21"
  availability_zone = "us-east-1c"

  tags = {
    Name = "${var.project_name}_${var.environment}_public_subnet_c_1"
  }
}

resource "aws_route_table" "rt_public_subnet_c_1" {
  vpc_id = aws_vpc.tf_resource_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.tf_resource_igw.id
  }

  tags = {
    Name = "${var.project_name}_${var.environment}_rt_public_subnet_c_1"
  }
}

resource "aws_route_table_association" "rta_public_subnet_c_1" {
  subnet_id      = aws_subnet.public_subnet_c_1.id
  route_table_id = aws_route_table.rt_public_subnet_c_1.id
}

############### private_subnet_d_1 ##################

resource "aws_subnet" "private_subnet_d_1" {
  vpc_id            = aws_vpc.tf_resource_vpc.id
  cidr_block        = "10.0.96.0/21"
  availability_zone = "us-east-1d"

  tags = {
    Name = "${var.project_name}_${var.environment}_private_subnet_d_1"
  }
}

resource "aws_route_table" "rt_private_subnet_d_1" {
  vpc_id = aws_vpc.tf_resource_vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.tf_resource_nat_1.id
  }

  tags = {
    Name = "${var.project_name}_${var.environment}_rt_private_subnet_d_1"
  }
}

resource "aws_route_table_association" "rta_private_subnet_d_1" {
  subnet_id      = aws_subnet.private_subnet_d_1.id
  route_table_id = aws_route_table.rt_private_subnet_d_1.id
}

############### public_subnet_d_1 ##################

resource "aws_subnet" "public_subnet_d_1" {
  vpc_id            = aws_vpc.tf_resource_vpc.id
  cidr_block        = "10.0.104.0/21"
  availability_zone = "us-east-1d"

  tags = {
    Name = "${var.project_name}_${var.environment}_public_subnet_d_1"
  }
}

resource "aws_route_table" "rt_public_subnet_d_1" {
  vpc_id = aws_vpc.tf_resource_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.tf_resource_igw.id
  }

  tags = {
    Name = "${var.project_name}_${var.environment}_rt_public_subnet_d_1"
  }
}

resource "aws_route_table_association" "rta_public_subnet_d_1" {
  subnet_id      = aws_subnet.public_subnet_d_1.id
  route_table_id = aws_route_table.rt_public_subnet_d_1.id
}

############### private_subnet_e_1 ##################

resource "aws_subnet" "private_subnet_e_1" {
  vpc_id            = aws_vpc.tf_resource_vpc.id
  cidr_block        = "10.0.128.0/21"
  availability_zone = "us-east-1e"

  tags = {
    Name = "${var.project_name}_${var.environment}_private_subnet_e_1"
  }
}

resource "aws_route_table" "rt_private_subnet_e_1" {
  vpc_id = aws_vpc.tf_resource_vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.tf_resource_nat_1.id
  }

  tags = {
    Name = "${var.project_name}_${var.environment}_rt_private_subnet_e_1"
  }
}

resource "aws_route_table_association" "rta_private_subnet_e_1" {
  subnet_id      = aws_subnet.private_subnet_e_1.id
  route_table_id = aws_route_table.rt_private_subnet_e_1.id
}

############### public_subnet_e_1 ##################

resource "aws_subnet" "public_subnet_e_1" {
  vpc_id            = aws_vpc.tf_resource_vpc.id
  cidr_block        = "10.0.136.0/21"
  availability_zone = "us-east-1e"

  tags = {
    Name = "${var.project_name}_${var.environment}_public_subnet_e_1"
  }
}

resource "aws_route_table" "rt_public_subnet_e_1" {
  vpc_id = aws_vpc.tf_resource_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.tf_resource_igw.id
  }

  tags = {
    Name = "${var.project_name}_${var.environment}_rt_public_subnet_e_1"
  }
}

resource "aws_route_table_association" "rta_public_subnet_e_1" {
  subnet_id      = aws_subnet.public_subnet_e_1.id
  route_table_id = aws_route_table.rt_public_subnet_e_1.id
}

############### private_subnet_f_1 ##################

resource "aws_subnet" "private_subnet_f_1" {
  vpc_id            = aws_vpc.tf_resource_vpc.id
  cidr_block        = "10.0.160.0/21"
  availability_zone = "us-east-1f"

  tags = {
    Name = "${var.project_name}_${var.environment}_private_subnet_f_1"
  }
}

resource "aws_route_table" "rt_private_subnet_f_1" {
  vpc_id = aws_vpc.tf_resource_vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.tf_resource_nat_1.id
  }

  tags = {
    Name = "${var.project_name}_${var.environment}_rt_private_subnet_f_1"
  }
}

resource "aws_route_table_association" "rta_private_subnet_f_1" {
  subnet_id      = aws_subnet.private_subnet_f_1.id
  route_table_id = aws_route_table.rt_private_subnet_f_1.id
}

############### public_subnet_f_1 ##################

resource "aws_subnet" "public_subnet_f_1" {
  vpc_id            = aws_vpc.tf_resource_vpc.id
  cidr_block        = "10.0.168.0/21"
  availability_zone = "us-east-1f"

  tags = {
    Name = "${var.project_name}_${var.environment}_public_subnet_f_1"
  }
}

resource "aws_route_table" "rt_public_subnet_f_1" {
  vpc_id = aws_vpc.tf_resource_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.tf_resource_igw.id
  }

  tags = {
    Name = "${var.project_name}_${var.environment}_rt_public_subnet_f_1"
  }
}

resource "aws_route_table_association" "rta_public_subnet_f_1" {
  subnet_id      = aws_subnet.public_subnet_f_1.id
  route_table_id = aws_route_table.rt_public_subnet_f_1.id
}
