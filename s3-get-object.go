package main

import (
	"log"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

func GetFileFromS3() *os.File {
	bucket := "testbucket"
	item := "iam-policy-structure.png"

	currentSession, _ := session.NewSession(&aws.Config{
		Region: aws.String("us-east-1")},
	)

	downloader := s3manager.NewDownloader(currentSession)

	file, _ := os.Create(item)
	_, errd := downloader.Download(file,
		&s3.GetObjectInput{
			Bucket: aws.String(bucket),
			Key:    aws.String(item),
		})

	if errd != nil {
		log.Fatalf("[ALARM] Unable to download item %q, %v", item, errd)
	}

	return file
}
